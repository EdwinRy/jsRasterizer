function Model()
{
    this.vertices = [];
    this.textureCoords = [];
    this.normals = [];
    this.indices = [];
}

function NewQuadModel(width, height)
{
    var model = new Model();
    hw = width*0.5;
    hh = width*0.5;
    
    model.vertices = 
    [
        hw,hh,
        -hw,hh,
        -hw,-hh,
        -hw,-hh,
        hw,-hh,
        hw,hh
    ];

    model.textureCoords = 
    [
        0,0,
        0,1,
        1,1,
        1,1,
        1,0,
        0,0
    ];
    return model;
}