function Vec2(x,y)
{
    this.x = x;
    this.y = y;

    this.Add = function(v)
    {
        this.x += v.x;
        this.y += v.y;
    }

    this.Sub = function(v)
    {
        this.x -= v.x;
        this.y -= v.y;
    }

    this.Mul = function(v)
    {
        this.x *= v.x;
        this.y *= v.y;
    }

    this.Div = function(v)
    {
        this.x /= v.x;
        this.y /= v.y;
    }

    this.Magnitude = function()
    {
        return Math.sqrt(this.x*this.x + this.y*this.y);
    }

    this.Normalise = function()
    {
        var len = this.Magnitude();
        if(len > 0)
        {
            invLen = 1/len;
            this.x *= invLen; this.y *= invLen;
        }
    }

    this.Dot = function(v)
    {
        return this.x*v.x + this.y*v.y;
    }

    this.Cross = function(v)
    {
        return this.x*v.y - this.y*v.x;
    }
}

function Vec3(x,y,z)
{
    this.x = x;
    this.y = y;
    this.z = z;

    this.Add = function(v)
    {
        this.x += v.x;
        this.y += v.y;
        this.z += v.z;
    }

    this.Sub = function(v)
    {
        this.x -= v.x;
        this.y -= v.y;
        this.z -= v.z;
    }

    this.Mul = function(v)
    {
        this.x *= v.x;
        this.y *= v.y;
        this.z *= v.z;
    }

    this.Div = function(v)
    {
        this.x /= v.x;
        this.y /= v.y;
        this.z /= v.z;
    }

    this.Magnitude = function()
    {
        return Math.sqrt(this.x*this.x + this.y*this.y + this.z*this.z);
    }

    this.Normalise = function()
    {
        var len = this.Magnitude();
        if(len > 0)
        {
            invLen = 1/len;
            this.x *= invLen; this.y *= invLen; this.z *= invLen;
        }
    }

    this.Dot = function(v)
    {
        return this.x*v.x + this.y*v.y + this.z*v.z;
    }

    this.Cross = function(v)
    {
        return new Vec3(
            this.y*v.z - this.z*v.y,
            this.z*v.x - this.x*v.z,
            this.x*v.y - this.y*v.x
        );
    }
}

//Matrices
function Mat3()
{
    this.data = [
        1,0,0,
        0,1,0,
        0,0,1
    ];

    this.Translate = function(x,y)
    {
        this.data[6] *= x;
        this.data[7] *= y;
    }

    this.Rotate = function(angleRadians)
    {
        var cosine = Math.cos(angleRadians);
        var sine = Math.sin(angleRadians);
        this.data[0] *= cosine;
        this.data[1] *= -sine;
        this.data[4] *= sine;
        this.data[5] *= cosine;
    }

    this.Scale = function(x,y)
    {
        this.data[0] *= x;
        this.data[4] *= y;
    }

    this.Mul = function(mat)
    {
        var temp = this.data.slice();
        this.data[0] = temp[0]*mat[0]+temp[1]*mat[3]+temp[2]*mat[6];
        this.data[1] = temp[0]*mat[1]+temp[1]*mat[4]+temp[2]*mat[7];
        this.data[2] = temp[0]*mat[2]+temp[1]*mat[5]+temp[2]*mat[8];

        this.data[3] = temp[3]*mat[0]+temp[4]*mat[3]+temp[5]*mat[6];
        this.data[4] = temp[3]*mat[1]+temp[4]*mat[4]+temp[5]*mat[7];
        this.data[5] = temp[3]*mat[2]+temp[4]*mat[5]+temp[5]*mat[8];

        this.data[6] = temp[6]*mat[0]+temp[7]*mat[3]+temp[8]*mat[6];
        this.data[7] = temp[6]*mat[1]+temp[7]*mat[4]+temp[8]*mat[7];
        this.data[8] = temp[6]*mat[2]+temp[7]*mat[5]+temp[8]*mat[8];
    }
}

function Mat4()
{
    this.data = [
        1,0,0,0,    //  0 , 1, 2, 3,
        0,1,0,0,    //  4 , 5, 6, 7,
        0,0,1,0,    //  8 , 9,10,11,
        0,0,0,1     //  12,13,14,15,
    ];

    this.Projection = function(fieldOfView,w,h,n,f)
    {
        var a = w/h;
        var fov = fieldOfView*Math.PI/180;
        var fov2 = Math.tan(Math.PI/2 - fov/2);
        var recipRange = 1/(n-f);
        
        this.data = [
            fov2/a,0,0,0,
            0,fov2,0,0,
            0,0,(n+f) * recipRange, -1,
            0,0,n*f*recipRange*2,1
        ];
    }

    this.Orthographic = function(w,h,z)
    {
        this.data = [
            2/w,0,0,0,
            0,-2/h,0,0,
            0,0,2/z,0,
            -1,1,0,1
        ];
    }

    this.Translation = function(x,y,z)
    {
        this.data[12] = x;
        this.data[13] = y;
        this.data[14] = z;
    }

    this.Rotation = function(x,y,z)
    {
        var temp = new Mat4();
        var cosine = Math.cos(x);
        var sine = Math.sin(x);
        temp.data[5] = cosine;
        temp.data[6] = sine;
        temp.data[9] = -sine;
        temp.data[10] = cosine;
        this.Multiply(temp);
        
        temp = new Mat4();
        cosine = Math.cos(y);
        sine = Math.sin(y);
        temp.data[0] = cosine;
        temp.data[2] = sine;
        temp.data[8] = -sine;
        temp.data[10] = cosine;
        this.Multiply(temp);

        temp = new Mat4();
        cosine = Math.cos(z);
        sine = Math.sin(z);
        temp.data[0] = cosine;
        temp.data[1] = sine;
        temp.data[4] = -sine;
        temp.data[5] = cosine;
        this.Multiply(temp);
    }

    this.RotationX = function(angleRadians)
    {
        var temp = new Mat4();
        var cosine = Math.cos(angleRadians);
        var sine = Math.sin(angleRadians);
        temp.data[5] += cosine;
        temp.data[6] += sine;
        temp.data[9] += -sine;
        temp.data[10] += cosine;
        this.Multiply(temp);
    }

    this.RotationY = function(angleRadians)
    {
        var cosine = Math.cos(angleRadians);
        var sine = Math.sin(angleRadians);
        this.data[0] *= cosine;
        this.data[2] *= -sine;
        this.data[8] *= sine;
        this.data[10] *= cosine;
    }

    this.RotationZ = function(angleRadians)
    {
        var cosine = Math.cos(angleRadians);
        var sine = Math.sin(angleRadians);
        this.data[0] *= cosine;
        this.data[1] *= sine;
        this.data[4] *= -sine;
        this.data[5] *= cosine;
    }

    this.Scale = function(x,y,z)
    {
        this.data[0] *= x;
        this.data[5] *= y;
        this.data[10] *= z;
    }

    this.Multiply = function(matrix)
    {
        var mat = matrix.data;
        var t = this.data.slice();
        this.data[0] = t[0]*mat[0]+t[1]*mat[4]+t[2]*mat[8]+t[3]*mat[12];
        this.data[1] = t[0]*mat[1]+t[1]*mat[5]+t[2]*mat[9]+t[3]*mat[13];
        this.data[2] = t[0]*mat[2]+t[1]*mat[6]+t[2]*mat[10]+t[3]*mat[14];
        this.data[3] = t[0]*mat[3]+t[1]*mat[7]+t[2]*mat[11]+t[3]*mat[15];

        this.data[4] = t[4]*mat[0]+t[5]*mat[4]+t[6]*mat[8]+t[7]*mat[12];
        this.data[5] = t[4]*mat[1]+t[5]*mat[5]+t[6]*mat[9]+t[7]*mat[13];
        this.data[6] = t[4]*mat[2]+t[5]*mat[6]+t[6]*mat[10]+t[7]*mat[14];
        this.data[7] = t[4]*mat[3]+t[5]*mat[7]+t[6]*mat[11]+t[7]*mat[15];

        this.data[8] = t[8]*mat[0]+t[9]*mat[4]+t[10]*mat[8]+t[11]*mat[12];
        this.data[9] = t[8]*mat[1]+t[9]*mat[5]+t[10]*mat[9]+t[11]*mat[13];
        this.data[10] = t[8]*mat[2]+t[9]*mat[6]+t[10]*mat[10]+t[11]*mat[14];
        this.data[11] = t[8]*mat[3]+t[9]*mat[7]+t[10]*mat[11]+t[11]*mat[15];

        this.data[12] = t[12]*mat[0]+t[13]*mat[4]+t[14]*mat[8]+t[15]*mat[12];
        this.data[13] = t[12]*mat[1]+t[13]*mat[5]+t[14]*mat[9]+t[15]*mat[13];
        this.data[14] = t[12]*mat[2]+t[13]*mat[6]+t[14]*mat[10]+t[15]*mat[14];
        this.data[15] = t[12]*mat[3]+t[13]*mat[7]+t[14]*mat[11]+t[15]*mat[15];
    }
}